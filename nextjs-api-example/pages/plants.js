import { useEffect, useState } from "react"
import Plant from "../components/Plant"
import { getAll, getAllFromAPI, getSearched, getSearchedFromAPI } from "../services/plant-fetcher"

export default function Plants() {
    const [listOfPlants, setListOfPlants] = useState()
    const [searchText, setSearchText] = useState("")
    const [searchColumn, setSearchColumn] = useState("name")
    useEffect(() => {
        // getAll().then(plantData => setListOfPlants(plantData))
        // getSearched(searchText, searchColumn).then(plantData => setListOfPlants(plantData))
        getSearchedFromAPI(searchText, searchColumn).then(setListOfPlants)
    }, [searchText, searchColumn])
    return <>
        <select value={searchColumn} onChange={(e) => setSearchColumn(e.target.value)}>
            <option value="name">Name</option>
            <option value="botanical_name">Botanical name</option>
        </select>
        <input value={searchText} onChange={(e) => setSearchText(e.target.value)} />
        Search: {searchText}
        {!listOfPlants ? "Loading..." :  listOfPlants?.map(plant =>
            <Plant name={plant.name} botanical_name={plant.botanical_name}
            />)}
    </>
}

