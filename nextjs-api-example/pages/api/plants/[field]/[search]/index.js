import { getSearched } from "../../../../../services/data"

export default function handler(req, res) {
    const {field, search} = req.query
    res.json(getSearched(search, field))
}