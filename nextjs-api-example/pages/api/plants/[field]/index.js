import { getSearched } from "../../../../services/data"

export default function handler(req, res) {
    const {field} = req.query
    const search = ""
    res.json(getSearched(search, field))
}