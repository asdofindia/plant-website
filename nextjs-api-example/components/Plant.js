export default function Plant({name, botanical_name}) {
    return <>
      <p>{name} <br/>{botanical_name}</p>
    </>
}