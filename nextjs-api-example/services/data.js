const fs = require('fs')

const csvtojson = require('csvtojson')

let DATA

csvtojson().fromFile('services/data.csv').then(data => {
    console.log(data)
    DATA = data
}
)

const getAll = () => DATA

const getSearched = (search, field) => DATA?.filter(plant => plant?.[field]?.toUpperCase().includes(search?.toUpperCase()))

module.exports = {
    getAll,
    getSearched
}