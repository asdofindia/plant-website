const ALL_PLANTS = [
    {
        "name": "Apple",
        "botanical_name": "Applyia apply"
    }, {
        "name": "orange",
        "botanical_name": "Orangish orang"
    }, {
        "name": "Neem",
        "botanical_name": "Neemish names"
    }, {
        "name": "Tulsi",
        "botanical_name": "Basil tulsia"
    }
]

export const getAll = async () => ALL_PLANTS


export const getSearched = async (search, field) => ALL_PLANTS?.filter(plant => plant?.[field]?.toUpperCase().includes(search?.toUpperCase()))

export const getAllFromAPI = () => fetch("/api/plants").then(res => res.json())

export const getSearchedFromAPI = (search, field) => fetch(`/api/plants/${field}/${search}`).then(res => res.json())
