const express = require('express');
const { getAll, getSearched } = require('./data');

const app = express()
const port = 4000

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:3000"); // update to match the domain you will make the request from
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


app.get('/plants', (req, res) => {
    res.send(
        getAll()
    )
})

app.get('/plants/:field/:search?', (req, res) => {
    const {search = "", field} = req.params
    res.send(getSearched(search, field))
})

app.listen(port, () => {
    console.log(`plants being served for consumption`)
})